import axios from 'axios';
import cheerio from 'cheerio';
import puppeteer from 'puppeteer';

const BASE_URL = 'https://kuramanime.net';

export const scrapeVideo = async({ list = [], id, slug, eps }) => {
    try {
        const browser = await puppeteer.launch({headless: true});
        const page = await browser.newPage();

        await page.goto(`${BASE_URL}/anime/${id}/${slug}/episode/${eps}/`, {waitUntil: 'networkidle2'});
        const data = await page.evaluate(() => document.querySelector('.video-content').innerHTML);
        browser.close();

        const $ = cheerio.load(data);
        $('.plyr__video-wrapper > video > source').each((i, el) => {
            list.push({
                res: parseInt($(el).attr('size')),
                source: $(el).attr('src'),
            });
        });
        return list;
    } catch (err) {
        console.log(err);
        return { error: err };
    }
};

export const scrapeVideoNew = async({ list = [], id, slug, eps }) => {
    try {
        const mainPage = await axios.get(`${BASE_URL}/anime/${id}/${slug}/episode/${eps}?activate_stream=1&stream_server=archive`);
        const $ = cheerio.load(mainPage.data);

        $('#animeVideoPlayer .video-content > video > source').each((i, el) => {
            list.push({
                res: parseInt($(el).attr('size')),
                source: $(el).attr('src'),
            });
        });
        return list;
    } catch (err) {
        console.log(err);
        return { error: err };
    }
};

export const scrapeLatestRelease = async({ list = [] }) => {
    try {
        const mainPage = await fetch(`${BASE_URL}/anime/ongoing?order_by=updated&need_json=true`);
        // const $ = cheerio.load(mainPage.data);

        // return mainPage.data.replace(/\n/g, '');
        // return mainPage.data.animes.data;

        mainPage.data.animes.data.forEach(function(val) {
            list.push({
                animeId: val.id,
                animeTitle: val.title,
                animeSlug: val.slug,
                animeEpisode: val.latest_episode,
                animeImg: val.image_portrait_url,
                animeRating: val.score,
                animeTotalEps: val.total_episodes,
                animePostDate: val.latest_post_at
            });
        });
        return list;
    } catch (err) {
        console.log(err);
        return { error: err };
    }
};

export const scrapeAnimeDetails = async({ id, slug }) => {
    try {
        let genres = [];

        const animePageTest = await axios.get(`${BASE_URL}/anime/${id}/${slug}/`);

        const $ = cheerio.load(animePageTest.data);

        // return animePageTest.data.replace(/\n/g, '');

        const animeTitle = $('.anime__details__title h3').text();
        const animeImage = $('meta[property=og:image]').prop('content');
        const type = $('.anime__details__widget ul > li:first-child > a').text();
        const desc = $('#synopsisField').text().replace('Catatan: Sinopsis diterjemahkan secara otomatis oleh Google Translate.', '');
        const releasedDate = $('.anime__details__widget ul > li:nth-child(4) > a').text().replace(' s/d?', '');
        const status = $('.anime__details__widget ul > li:nth-child(3) > a').text();
        const totalEps = $('.anime__details__widget ul > li:nth-child(2) > a').text();
        const studio = $('.anime__details__widget ul > li:nth-child(14) > a').text().replace(/\n/g, '');
        const durasi = $('.anime__details__widget ul > li:nth-child(6) > a').text().replace(/\n/g, '');
        const peminat = $('.anime__details__widget ul > li:nth-child(16) > a').text().replace(/\n/g, '').replace(',', '.');
        const rating = $('.anime__details__widget ul > li:nth-child(15) > a').text().replace(' / 10.00').replace('undefined', '');

        $('.anime__details__widget ul > li:nth-child(10) > a').each((i, elem) => {
            genres.push($(elem).text().replace(',', '').trim());
        });

        var arrayEps = [];
        $($.parseHTML($('#episodeLists').data('content'))).filter('a').each((i, el) => {
            const epsId = parseInt($(el).text().replace(/\n/g, '').replace(/\s/g,'').replace('Ep', '').replace('(Terlama)', '').replace('(Terbaru)', ''));

            if(epsId > 0){
                arrayEps.push(epsId);
            }
        });

        arrayEps.sort(function(a,b){
            if(a > b){ return 1}
            if(a < b){ return -1}
               return 0;
        });

        return {
            animeTitle: animeTitle.toString(),
            animeRating: rating.toString(),
            type: type.toString(),
            releasedDate: releasedDate.toString(),
            status: status.toString(),
            totalEps: totalEps.toString(),
            studio: studio.toString(),
            peminat: peminat.toString(),
            durasi: durasi.toString(),
            genres: genres,
            synopsis: desc.toString(),
            animeImg: animeImage.toString(),
            firstEps: arrayEps[0],
            lastEps: arrayEps[arrayEps.length-1],
        };
    } catch (err) {
        console.log(err);
        return { error: err };
    }
};

export const scrapeEpisodeDetails = async({ id, slug, eps }) => {
    try {
        const animePageTest = await axios.get(`${BASE_URL}/anime/${id}/${slug}/episode/${eps}?activate_stream=1&stream_server=archive`);

        const $ = cheerio.load(animePageTest.data);

        // return animePageTest.data.replace(/\n/g, '');

        var listVideo = [];

        $('#animeVideoPlayer .video-content  video#player > source').each((i, el) => {
            listVideo.push({
                res: parseInt($(el).attr('size')),
                source: $(el).attr('src'),
            });
        });

        const animeTitle = $('.breadcrumb-option .col-lg-12:nth-child(2) > .breadcrumb__links > a:nth-child(3)').text();
        const episodeTitle = $('.breadcrumb__links #episodeTitle').text().replace(' Subtitle Indonesia', '');
        const releaseDate = $('.breadcrumb-option .col-lg-12:nth-child(3) > .breadcrumb__links__v2 > span:last-child').text().match(/\((.*)\)/);

        return {
            animeTitle: animeTitle.toString(),
            episodeTitle: episodeTitle.toString(),
            releaseDate: releaseDate[0].toString().replace('(sekitar ', '').replace(')', ''),
            listVideo: listVideo
        };
    } catch (err) {
        console.log(err);
        return { error: err };
    }
};

export const scrapeSearch = async({ list = [], keyw, page = 1 }) => {
    try {
        const searchPage = await axios.get(
            `${BASE_URL}/anime?order_by=latest&page=${page}&search=${keyw}&need_json=true`
        );
        // const $ = cheerio.load(searchPage.data);
        // return searchPage.data.replace(/\n/g, '');

        searchPage.data.animes.data.forEach(function(val) {
            list.push({
                animeId: val.id,
                animeTitle: val.title,
                animeSlug: val.slug,
                animeEpisode: val.latest_episode,
                animeImg: val.image_portrait_url,
                animeRating: val.score,
                animeTotalEps: val.total_episodes,
                animePostDate: val.latest_post_at
            });
        });

        return list;
    } catch (err) {
        console.log(err);
        return { error: err };
    }
};

export const scrapeSchedule = async({ list = [], day }) => {
    try {
        const searchPage = await axios.get(
            `${BASE_URL}/schedule?scheduled_day=${day}&need_json=true`
        );
        // const $ = cheerio.load(searchPage.data);
        // return searchPage.data.replace(/\n/g, '');

        searchPage.data.animes.data.forEach(function(val) {
            list.push({
                animeId: val.id,
                animeTitle: val.title,
                animeSlug: val.slug,
                animeEpisode: val.latest_episode,
                animeImg: val.image_portrait_url,
                animeRating: val.score,
                animeTotalEps: val.total_episodes,
                animePostDate: val.latest_post_at
            });
        });

        return list;
    } catch (err) {
        console.log(err);
        return { error: err };
    }
};

export const scrapeGenre = async({ list = [] }) => {
    try {
        const searchPage = await axios.get(
            `${BASE_URL}/properties/genre`
        );
        const $ = cheerio.load(searchPage.data);
        // return searchPage.data.replace(/\n/g, '');

        $('.kuramanime__genres > ul > li').each((i, el) => {
            list.push({
                genreSlug: $(el).find('a').attr('href').split('/')[5].replace('?order_by=text&from_property=1', ''),
                genreTitle: $(el).find('a > span').text(),
            });
        });

        return list;
    } catch (err) {
        console.log(err);
        return { error: err };
    }
};

export const scrapeSearchGenre = async({ list = [], genre, page }) => {
    try {
        const searchPage = await axios.get(
            `${BASE_URL}/properties/genre/${genre}?order_by=latest&page=${page}&need_json=true`
        );
        // const $ = cheerio.load(searchPage.data);
        // return searchPage.data.replace(/\n/g, '');

        searchPage.data.animes.data.forEach(function(val) {
            list.push({
                animeId: val.id,
                animeTitle: val.title,
                animeSlug: val.slug,
                animeEpisode: val.latest_episode,
                animeImg: val.image_portrait_url,
                animeRating: val.score,
                animeTotalEps: val.total_episodes,
                animePostDate: val.latest_post_at
            });
        });

        return list;
    } catch (err) {
        console.log(err);
        return { error: err };
    }
};

export const scrapeAnimeMovies = async({ list = [], page = 1 }) => {
    try {
        const searchPage = await axios.get(
            `${BASE_URL}/anime/movie?order_by=updated&page=${page}&need_json=true`
        );
        // const $ = cheerio.load(searchPage.data);
        // return searchPage.data.replace(/\n/g, '');

        searchPage.data.animes.data.forEach(function(val) {
            list.push({
                animeId: val.id,
                animeTitle: val.title,
                animeSlug: val.slug,
                animeEpisode: val.latest_episode,
                animeImg: val.image_portrait_url,
                animeRating: val.score,
                animeTotalEps: val.total_episodes,
                animePostDate: val.latest_post_at
            });
        });

        return list;
    } catch (err) {
        console.log(err);
        return { error: err };
    }
};

export const scrapeMovieDetails = async({ id, slug }) => {
    try {
        let genres = [];

        const animePageTest = await axios.get(`${BASE_URL}/anime/${slug}/`);

        const $ = cheerio.load(animePageTest.data);

        // return animePageTest.data.replace(/\n/g, '');

        const animeTitle = $('.bigcontent > .infox > .alter').text();
        const animeImage = $('.bigcontent > .thumb > img').data('src').replace(/\s/g,'');
        const type = $('.bigcontent > .infox > .spe > span:nth-child(5)').text().replace('Jenis: ', '');
        const desc = $('.sinopsis .desc > p:first-child').text().replace('[Ditulis oleh Animasu Menulis kembali]', '');
        const releasedDate = $('.bigcontent > .infox > .spe > span:nth-child(4)').text().replace('Rilis: ', '');
        const durationEps = $('.bigcontent > .infox > .spe > span:nth-child(6)').text().replace('Durasi: ', '');
        const status = $('.bigcontent > .infox > .spe > span:nth-child(3)').text().replace('Status: ', '').replace(' ✓', '');
        const rating = $('.rt > .rating > strong').text().replace('Rating ', '');

        $('.bigcontent > .infox > .spe > span:first-child > a').each((i, elem) => {
            genres.push({
                title: $(elem).text(),   
                slug: $(elem).attr('href').split('/')[4],   
            });
        });

        return {
            animeTitle: animeTitle.toString(),
            animeRating: rating.toString(),
            type: type.toString(),
            status: status,
            animeImg: animeImage.toString(),
            releasedDate: releasedDate.toString(),
            durationEps: durationEps.toString(),
            genres: genres,
            synopsis: desc.toString(),
            slug: $('#daftarepisode > li').find('.lchx > a').attr('href').split('/')[3]
        };
    } catch (err) {
        console.log(err);
        return { error: err };
    }
};