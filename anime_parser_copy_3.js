import axios from 'axios';
import cheerio from 'cheerio';
import atob from 'atob';
import { extractFembed } from './helpers/extractors/fembed.js';

const BASE_URL = 'https://animasu.nl';

export const scrapeLatestRelease = async({ list = [] }) => {
    try {
        const mainPage = await axios.get(`${BASE_URL}/sedang-tayang`);
        const $ = cheerio.load(mainPage.data);

        // return mainPage.data.replace(/\n/g, '');

        $('.page > .listupd > .bs').each((i, el) => {
            list.push({
                animeId: $(el).find('.bsx > a').attr('href').split('/')[4],
                animeTitle: $(el).find('.bsx > a > .tt').text().replace(/\n/g, '').replace(' ', ''),
                animeSlug: $(el).find('.bsx > a').attr('href').split('/')[4],
                animeImg: $(el).find('.bsx > a > .limit > img').data('src').replace(/\n/g, ''),
                animeType: $(el).find('.bsx > a > .limit > .typez').text(),
                animeEpisode: $(el).find('.bsx > a > .limit > .bt > .epx').text(),
            });
        });
        return list;
    } catch (err) {
        console.log(err);
        return { error: err };
    }
};

export const scrapeAnimeDetails = async({ slug }) => {
    try {
        let genres = [];
        let listEps = [];

        const animePageTest = await axios.get(`${BASE_URL}/anime/${slug}/`);

        const $ = cheerio.load(animePageTest.data);

        // return animePageTest.data.replace(/\n/g, '');

        var animeTitle = $('.bigcontent > .infox > .alter').text();
        var animeImage = $('.bigcontent > .thumb > img').data('src').replace(/\s/g,'');
        var type = "";
        var desc = $('.sinopsis .desc > p:first-child').text().replace('[Ditulis oleh Animasu Menulis kembali]', '');
        var releasedDate = "";
        var durationEps = "";
        var status = "";
        var studio = "";
        var lastUpdate = "";
        var rating = $('.rt > .rating > strong').text().replace('Rating ', '');

        $('.bigcontent > .infox > .spe > span').each((i, elm) => {
            const textData = $(elm).text();
            if(textData.includes('Jenis:') == true){
                type = textData.replace('Jenis: ', '');
            }
            if(textData.includes('Rilis:') == true){
                releasedDate = textData.replace('Rilis: ', '');
            }
            if(textData.includes('Durasi:') == true){
                durationEps = textData.replace('Durasi: ', '');
            }
            if(textData.includes('Status:') == true){
                status = textData.replace('Status: ', '').replace(' 🔥', '');
            }
            if(textData.includes('Studio:') == true){
                studio = textData.replace('Studio: ', '');
            }
            if(textData.includes('Diupdate:') == true){
                lastUpdate = textData.replace('Diupdate: ', '');
            }
        });

        $('.bigcontent > .infox > .spe > span:first-child > a').each((i, elem) => {
            genres.push({
                title: $(elem).text(),   
                slug: $(elem).attr('href').split('/')[4],   
            });
        });

        $('#daftarepisode > li').each((i, elem) => {
            listEps.push({
                title: $(elem).find('.lchx > a').text(),
                slug: $(elem).find('.lchx > a').attr('href').split('/')[3],
            });
        });

        return {
            animeTitle: animeTitle.toString(),
            animeRating: rating.toString(),
            type: type.toString(),
            studio: studio.toString(),
            lastUpdate: lastUpdate.toString(),
            status: status,
            animeImg: animeImage.toString(),
            releasedDate: releasedDate.toString(),
            durationEps: durationEps.toString(),
            genres: genres,
            synopsis: desc.toString(),
            listEps: listEps
        };
    } catch (err) {
        console.log(err);
        return { error: err };
    }
};

export const scrapeEpisodeDetails = async({ slug }) => {
    try {
        const animePageTest = await axios.get(`${BASE_URL}/${slug}/`);

        const $ = cheerio.load(animePageTest.data);

        // return animePageTest.data.replace(/\n/g, '');

        const animeTitle = $('.item.meta > .lm > .epx > a').text();
        const animeSlug = $('.item.meta > .lm > .epx > a').attr('href').split('/')[4];
        const episodeTitle = $('.item.meta > .lm > h1').text().replace('Nonton ', '').replace(' Sub Indo', '').replace(animeTitle+' ', '');
        const releaseDate = $('.item.meta > .lm > .year').text().replace('  ', '');
        const animeImage = $('.item.meta > .tb > img').data('src').replace(/\n/g, '');

        var videoList = [];
        $('.video-nav .mirror > option').each(async (i, el) => {
            var videoDecrypt = atob($(el).val());
            const videoSrc = $($.parseHTML(videoDecrypt)).attr('src');
            if(videoSrc){
                if(videoSrc.includes("fembed") == true){
                    const sources = await extractFembed(videoSrc);
                    videoList.push({
                        videoText: $(el).text().replace(/\s/g,'').replace('[1]', '').replace('[2]', ' alt').replace('[HDTS]', ''),
                        videUrl: sources
                    });
                }
            }
        });


        return {
            animeTitle: animeTitle.toString(),
            animeSlug: animeSlug.toString(),
            episodeTitle: episodeTitle.toString(),
            releaseDate: releaseDate.toString(),
            animeImage: animeImage.toString(),
            videoSrc: videoList
        };
    } catch (err) {
        console.log(err);
        return { error: err };
    }
};

export const scrapeSearch = async({ list = [], keyw, page = 1 }) => {
    try {
        const searchPage = await axios.get(
            `${BASE_URL}/page/${page}/?s=${keyw}`
        );
        const $ = cheerio.load(searchPage.data);
        // return searchPage.data.replace(/\n/g, '');

        $('.listupd > .bs').each((i, el) => {
            list.push({
                animeId: $(el).find('.bsx > a').attr('href').split('/')[4],
                animeTitle: $(el).find('.bsx > a > .tt').text().replace(/\n/g, ''),
                animeSlug: $(el).find('.bsx > a').attr('href').split('/')[4],
                animeImg: $(el).find('.bsx > a > .limit > img').data('src').replace(/\n/g, ''),
                animeType: $(el).find('.bsx > a > .limit > .typez').text(),
                animeEpisode: $(el).find('.bsx > a > .limit > .bt > .epx').text(),
            });
        });

        return list;
    } catch (err) {
        console.log(err);
        return { error: err };
    }
};

export const scrapeSchedule = async({ list = [] }) => {
    try {
        const searchPage = await axios.get(
            `${BASE_URL}/jadwal`
        );
        const $ = cheerio.load(searchPage.data);
        // return searchPage.data.replace(/\n/g, '');

        $('.postbody > .bixbox').each((i, el) => {
            var animeList = [];
            const day = $(el).find('.releases > h3 > span').text();
            $(el).find('.listupd > .bs').each((i, el) => {
                animeList.push({
                    animeId: $(el).find('.bsx > a').attr('href').split('/')[4],
                    animeTitle: $(el).find('.bsx > a > .tt').text().replace(/\n/g, ''),
                    animeSlug: $(el).find('.bsx > a').attr('href').split('/')[4],
                    animeImg: $(el).find('.bsx > a > .limit > img').data('src').replace(/\n/g, ''),
                    animeCountdown: $(el).find('.bsx > a > .limit > .bt > .cndwn').text().replace('Sudah Rilis!', 'Tunggu...') || 'Sudah Rilis!',
                });
            });
            if(day != ""){
                list.push({
                    day: day,
                    animeList: animeList
                });
            }
        });

        return list;
    } catch (err) {
        console.log(err);
        return { error: err };
    }
};

export const scrapeGenre = async({ list = [] }) => {
    try {
        const searchPage = await axios.get(
            `${BASE_URL}/genre-anime`
        );
        const $ = cheerio.load(searchPage.data);
        // return searchPage.data.replace(/\n/g, '');

        $('.genrepage > a').each((i, el) => {
            list.push({
                genreSlug: $(el).attr('href').split('/')[4],
                genreTitle: $(el).text(),
            });
        });

        return list;
    } catch (err) {
        console.log(err);
        return { error: err };
    }
};

export const scrapeSearchGenre = async({ list = [], genre, page }) => {
    try {
        const searchPage = await axios.get(
            `${BASE_URL}/genre/${genre}/page/${page}`
        );
        const $ = cheerio.load(searchPage.data);
        // return searchPage.data.replace(/\n/g, '');

        $('.listupd > .bs').each((i, el) => {
            list.push({
                animeId: $(el).find('.bsx > a').attr('href').split('/')[4],
                animeTitle: $(el).find('.bsx > a > .tt').text().replace(/\n/g, ''),
                animeSlug: $(el).find('.bsx > a').attr('href').split('/')[4],
                animeImg: $(el).find('.bsx > a > .limit > img').data('src').replace(/\n/g, ''),
                animeType: $(el).find('.bsx > a > .limit > .typez').text(),
                animeEpisode: $(el).find('.bsx > a > .limit > .bt > .epx').text(),
            });
        });

        return list;
    } catch (err) {
        console.log(err);
        return { error: err };
    }
};

export const scrapeAnimeMovies = async({ list = [], page = 1 }) => {
    try {
        const searchPage = await axios.get(
            `${BASE_URL}/movie/?halaman=${page}`
        );
        const $ = cheerio.load(searchPage.data);
        // return searchPage.data.replace(/\n/g, '');

        $('.listupd > .bs').each((i, el) => {
            list.push({
                animeId: $(el).find('.bsx > a').attr('href').split('/')[4],
                animeTitle: $(el).find('.bsx > a > .tt').text().replace(/\n/g, ''),
                animeSlug: $(el).find('.bsx > a').attr('href').split('/')[4],
                animeImg: $(el).find('.bsx > a > .limit > img').data('src').replace(/\n/g, ''),
                animeRating: $(el).find('.bsx > a > .limit > .typez').text(),
                animeRilis: $(el).find('.bsx > a > .limit > .bt > .epx').text(),
            });
        });

        return list;
    } catch (err) {
        console.log(err);
        return { error: err };
    }
};

export const scrapeMovieDetails = async({ slug }) => {
    try {
        let genres = [];

        const animePageTest = await axios.get(`${BASE_URL}/anime/${slug}/`);

        const $ = cheerio.load(animePageTest.data);

        // return animePageTest.data.replace(/\n/g, '');

        const animeTitle = $('.bigcontent > .infox > .alter').text();
        const animeImage = $('.bigcontent > .thumb > img').data('src').replace(/\s/g,'');
        const type = $('.bigcontent > .infox > .spe > span:nth-child(5)').text().replace('Jenis: ', '');
        const desc = $('.sinopsis .desc > p:first-child').text().replace('[Ditulis oleh Animasu Menulis kembali]', '');
        const releasedDate = $('.bigcontent > .infox > .spe > span:nth-child(4)').text().replace('Rilis: ', '');
        const durationEps = $('.bigcontent > .infox > .spe > span:nth-child(6)').text().replace('Durasi: ', '');
        const status = $('.bigcontent > .infox > .spe > span:nth-child(3)').text().replace('Status: ', '').replace(' ✓', '');
        const rating = $('.rt > .rating > strong').text().replace('Rating ', '');

        $('.bigcontent > .infox > .spe > span:first-child > a').each((i, elem) => {
            genres.push({
                title: $(elem).text(),   
                slug: $(elem).attr('href').split('/')[4],   
            });
        });

        return {
            animeTitle: animeTitle.toString(),
            animeRating: rating.toString(),
            type: type.toString(),
            status: status,
            animeImg: animeImage.toString(),
            releasedDate: releasedDate.toString(),
            durationEps: durationEps.toString(),
            genres: genres,
            synopsis: desc.toString(),
            slug: $('#daftarepisode > li').find('.lchx > a').attr('href').split('/')[3]
        };
    } catch (err) {
        console.log(err);
        return { error: err };
    }
};