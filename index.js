import axios from 'axios';
import express from 'express';
import cors from 'cors';
import mongoose from 'mongoose';
import savedAnime from './model/savedAnimeModel.js';
import comments from './model/commentsModel.js';

import {
    scrapeGenre,
    scrapeAnimeMovies,
    scrapeSearch,
    scrapeAnimeDetails,
    scrapeLatestRelease,
    scrapeEpisodeDetails,
    scrapeSchedule,
    scrapeSearchGenre,
    scrapeMovieDetails,
    scrapeVideoNew
} from './anime_parser.js';

const port = process.env.PORT || 3001;

const corsOptions = {
    origin: '*',
    credentials: true,
    optionSuccessStatus: 200,
    port: port,
};

const app = express();

function connectDB(){
    const uri = "mongodb+srv://thomy:super10Admin@serverlessinstance0.up4cr.mongodb.net/?retryWrites=true&w=majority";
    mongoose.set("strictQuery", false);
    const dbconn = mongoose.connect(uri, {useNewUrlParser: true}, function(err) {
        if (err) {
            console.log("Mongo DB can't connected");
            connectDB();
        }
    });
    const connection = mongoose.connection;
    connection.once('open', () => {
        console.log("Mongo DB established");
    })
    connection.on('error', err => {
        console.log("Could not connect to mongo server!");
        connectDB();
    });
}
connectDB();

app.use(cors(corsOptions));
app.use(express.json());

app.get('/', (req, res) => {
    res.status(200).json('Welcome to AnimeKool API!');
});

app.post('/save-comments', async(req, res) => {
    try {
        const data = {
            user_id: req.body.user_id,
            anime_id: req.body.anime_id,
            episode: req.body.episode,
            message: req.body.message,
            parent: req.body.parent,
        };

        await comments.create(data);

        res.status(200).json(data);
    } catch (err) {
        res.status(500).json({
            status: 500,
            error: 'Internal Error',
            message: err,
        });
    }
});

app.get('/get-comment/:id/:episode', async(req, res) => {
    try {
        const id = req.params.id;
        const episode = req.params.episode;

        const data = await comments.find({anime_id: id, episode: episode}).sort( { createdAt: -1 } );

        res.status(200).json(data);
    } catch (err) {
        res.status(500).json({
            status: 500,
            error: 'Internal Error',
            message: err,
        });
    }
});

app.post('/save-anime', async(req, res) => {
    try {
        const data = {
            user_id: req.body.user_id,
            anime_id: req.body.anime_id,
            data: req.body.data,
            status: req.body.status,
        };

        await savedAnime.findOneAndUpdate({user_id: req.body.user_id, anime_id: req.body.anime_id}, data, {upsert: true});

        res.status(200).json(data);
    } catch (err) {
        res.status(500).json({
            status: 500,
            error: 'Internal Error',
            message: err,
        });
    }
});

app.post('/save-detail', async(req, res) => {
    try {
        const data = await savedAnime.findOne({user_id: req.body.user_id, anime_id: req.body.anime_id});

        res.status(200).json(data);
    } catch (err) {
        res.status(500).json({
            status: 500,
            error: 'Internal Error',
            message: err,
        });
    }
});

app.get('/saved-all/:user', async(req, res) => {
    try {
        const data = await savedAnime.find({user_id: req.params.user});

        res.status(200).json(data);
    } catch (err) {
        res.status(500).json({
            status: 500,
            error: 'Internal Error',
            message: err,
        });
    }
});

app.get('/data-apps', async(req, res) => {
    try {
        const data = {
            'app_name': 'Animekool',
            'app_version': '1.0'
        };

        res.status(200).json(data);
    } catch (err) {
        res.status(500).json({
            status: 500,
            error: 'Internal Error',
            message: err,
        });
    }
});

app.get('/get-video-new/:id/:slug/:episode', async(req, res) => {
    try {
        const id = req.params.id;
        const slug = req.params.slug;
        const episode = req.params.episode;

        const data = await scrapeVideoNew({id:id, slug:slug, eps:episode});

        res.status(200).json(data);
    } catch (err) {
        res.status(500).json({
            status: 500,
            error: 'Internal Error',
            message: err,
        });
    }
});

app.get('/latest-release', async(req, res) => {
    try {
        const data = await scrapeLatestRelease({});

        res.status(200).json(data);
    } catch (err) {
        res.status(500).json({
            status: 500,
            error: 'Internal Error',
            message: err,
        });
    }
});

app.get('/anime-details/:id/:slug', async(req, res) => {
    try {
        const id = req.params.id;
        const slug = req.params.slug;

        const data = await scrapeAnimeDetails({ id:id, slug: slug });

        res.status(200).json(data);
    } catch (err) {
        res.status(500).json({
            status: 500,
            error: 'Internal Error',
            message: err,
        });
    }
});

app.get('/episode-details/:id/:slug/:episode', async(req, res) => {
    try {
        const id = req.params.id;
        const slug = req.params.slug;
        const episode = req.params.episode;

        const data = await scrapeEpisodeDetails({id:id, slug:slug, eps:episode});

        res.status(200).json(data);
    } catch (err) {
        res.status(500).json({
            status: 500,
            error: 'Internal Error',
            message: err,
        });
    }
});

app.get('/search', async(req, res) => {
    try {
        const keyw = req.query.keyw;
        const page = req.query.page;

        const data = await scrapeSearch({ keyw: keyw, page: page });

        res.status(200).json(data);
    } catch (err) {
        res.status(500).json({
            status: 500,
            error: 'Internal Error',
            message: err,
        });
    }
});

app.get('/schedule/:day', async(req, res) => {
    try {
        const day = req.params.day;
        const data = await scrapeSchedule({day:day});

        res.status(200).json(data);
    } catch (err) {
        res.status(500).json({
            status: 500,
            error: 'Internal Error',
            message: err,
        });
    }
});

app.get('/genre', async(req, res) => {
    try {
        const data = await scrapeGenre({});

        res.status(200).json(data);
    } catch (err) {
        res.status(500).send({
            status: 500,
            error: 'Internal Error',
            message: err,
        });
    }
});

app.get('/genre/:genre/:page', async(req, res) => {
    try {
        const genre = req.params.genre;
        const page = req.params.page;
        const data = await scrapeSearchGenre({ genre: genre, page: page });

        res.status(200).json(data);
    } catch (err) {
        res.status(500).send({
            status: 500,
            error: 'Internal Error',
            message: err,
        });
    }
});

app.get('/anime-movies/:page', async(req, res) => {
    try {
        const page = req.params.page;

        const data = await scrapeAnimeMovies({ page: page});

        res.status(200).json(data);
    } catch (err) {
        res.status(500).send({
            status: 500,
            error: 'Internal Error',
            message: err,
        });
    }
});

app.get('/movie-details/:slug', async(req, res) => {
    try {
        const slug = req.params.slug;

        const data = await scrapeMovieDetails({ slug: slug });

        res.status(200).json(data);
    } catch (err) {
        res.status(500).json({
            status: 500,
            error: 'Internal Error',
            message: err,
        });
    }
});

app.use((req, res) => {
    res.status(404).json({
        status: 404,
        error: 'Not Found',
    });
});

app.listen(port, () => {
    console.log('Express server listening on port %d in %s mode', port, app.settings.env);
});