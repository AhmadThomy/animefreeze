import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const table = 'saved_anime';
const dataSchema = new Schema({
    user_id: {type: String, required: true},
    anime_id: {type: String, required: true},
    data: {type: String, required: true},
    status: {type: Number, required: true},
}, {
    timestamps: true,
});

const savedAnime = mongoose.model(table, dataSchema)
export default savedAnime;