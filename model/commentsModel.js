import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const table = 'comments';
const dataSchema = new Schema({
    user_id: {type: String, required: true},
    anime_id: {type: String, required: true},
    episode: {type: Number, required: true},
    message: {type: String, required: true},
    parent: {type: String, required: false},
}, {
    timestamps: true,
});

const comments = mongoose.model(table, dataSchema)
export default comments;